//
//  GameViewController.swift
//  Tic-Tac-Toe
//
//  Created by Virginia on 11/28/19.
//  Copyright © 2019 South Dakota School of Mines. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    @IBOutlet weak var dumbButton: UIButton!
    var turn = 0
    var state = [0,0,0,0,0,0,0,0,0]
    var winner = false
    
    @IBOutlet weak var winnerLabel: UILabel!
    
    
    @IBAction func reset(_ sender: Any) {
        winner = false
        turn = 0
        
        // Reset game state
        state[0] = 0
        state[1] = 0
        state[2] = 0
        state[3] = 0
        state[4] = 0
        state[5] = 0
        state[6] = 0
        state[7] = 0
        state[8] = 0
        
        // Reset images on buttons
        
        let button1 = view.viewWithTag(1) as! UIButton
        button1.setImage(nil, for: UIControlState())
        dumbButton.setImage(nil, for: UIControlState())
        let button3 = view.viewWithTag(3) as! UIButton
        button3.setImage(nil, for: UIControlState())
        let button4 = view.viewWithTag(4) as! UIButton
        button4.setImage(nil, for: UIControlState())
        let button5 = view.viewWithTag(5) as! UIButton
        button5.setImage(nil, for: UIControlState())
        let button6 = view.viewWithTag(6) as! UIButton
        button6.setImage(nil, for: UIControlState())
        let button7 = view.viewWithTag(7) as! UIButton
        button7.setImage(nil, for: UIControlState())
        let button8 = view.viewWithTag(8) as! UIButton
        button8.setImage(nil, for: UIControlState())
        let button9 = view.viewWithTag(9) as! UIButton
        button9.setImage(nil, for: UIControlState())

        
    
        
        winnerLabel.text = "Tic Tac Toe"
    }
    
    @IBAction func mark(_ sender: AnyObject) {
        if(winner == false && state[sender.tag-1] == 0){
            if (turn % 2 == 0){
                sender.setImage(UIImage(named: "cross.jpg"), for: UIControlState())
                state[(sender as AnyObject).tag-1] = 1
            }
            else{
                sender.setImage(UIImage(named: "nought.jpg"), for: UIControlState())
                state[sender.tag-1] = 2
            }
            turn = turn + 1
        }
        
        // Check for winning combinations
        if(winner == false){
            // Check if cross wins
            if((state[0] == 1 && state[1] == 1 && state[2] == 1) || (state[3] == 1 && state[4] == 1 && state[5] == 1) || (state[6] == 1 && state[7] == 1 && state[8] == 1) || (state[0] == 1 && state[3] == 1 && state[6] == 1) || (state[1] == 1 && state[4] == 1 && state[7] == 1) || (state[2] == 1 && state[5] == 1 && state[8] == 1) || (state[0] == 1 && state[4] == 1 && state[8] == 1) || (state[2] == 1 && state[4] == 1 && state[6] == 1)) {
                
                print("Cross wins")
                winner = true
                winnerLabel.text = "Player 1 Wins!"
            }
            // Check if nought wins
            else if((state[0] == 2 && state[1] == 2 && state[2] == 2) || (state[3] == 2 && state[4] == 2 && state[5] == 2) || (state[6] == 2 && state[7] == 2 && state[8] == 2) || (state[0] == 2 && state[3] == 2 && state[6] == 2) || (state[1] == 2 && state[4] == 2 && state[7] == 2) || (state[2] == 2 && state[5] == 2 && state[8] == 2) || (state[0] == 1 && state[4] == 2 && state[8] == 2) || (state[2] == 2 && state[4] == 2 && state[6] == 2)) {
                
                print("Nought wins")
                winner = true
                winnerLabel.text = "Player 2 Wins!"
            }
            // See if Cat's game
            else {
                var cat = true
                for i in 0...8 {
                    if(state[i] == 0) {
                        cat = false
                    }
                }
                if(cat == true) {
                    print("Cat's Game")
                    winner = true
                    winnerLabel.text = "Cat's Game :("
                }
            }
        }
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "GameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
